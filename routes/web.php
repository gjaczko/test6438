<?php

use App\Livewire\Areas\CreateArea;
use App\Livewire\Areas\EditArea;
use App\Livewire\Areas\ListAreas;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', ListAreas::class)->name('areas.list');
Route::get('/areas/new', CreateArea::class)->name('areas.create');
Route::get('/areas/{area}', EditArea::class)->name('areas.edit');
