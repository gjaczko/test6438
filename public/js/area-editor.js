/**
 * Area Editor for interfacing with Leaflet Draw and exposing the geometry as GeoJSON.
 *
 * @class AreaEditor
 */
class AreaEditor {

    #mapId;
    #initialGeometry;
    #onCreated;
    #onEdited;
    #onDeleted;

    #map;
    #editableLayer;

    /**
     * @param {string} mapId - The ID of the element to render the map in.
     * @param {object} initialGeometry - The initial geometry object.
     * @param {function} onCreated - Callback function triggered when a new area is created.
     * @param {function} onEdited - Callback function triggered when an existing area is edited.
     * @param {function} onDeleted - Callback function triggered when an existing area is deleted.
     */
    constructor({mapId, initialGeometry, onCreated, onEdited, onDeleted}) {

        this.#mapId = mapId;
        this.#initialGeometry = initialGeometry;
        this.#onCreated = onCreated;
        this.#onEdited = onEdited;
        this.#onDeleted = onDeleted;

        // Initialise the map
        this.#map = L.map(this.#mapId).setView([54.5, -3], 5);

        // Add the OpenStreetMap tiles
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>'
        }).addTo(this.#map);

        // Initialise the editable layer
        this.#editableLayer = this.#initialGeometry ? L.geoJSON(this.#initialGeometry) : new L.FeatureGroup();
        this.#map.addLayer(this.#editableLayer);
        if (this.#initialGeometry) {
            this.#map.fitBounds(this.#editableLayer.getBounds());
        }

        // Initialise the draw control
        const drawControl = new L.Control.Draw({
            draw: {
                polygon: true,
                polyline: false,
                rectangle: true,
                circle: false,
                circlemarker: false,
                marker: false
            },
            edit: {
                featureGroup: this.#editableLayer,
            }
        });
        this.#map.addControl(drawControl);

        // Remove existing features when drawing new ones
        this.#map.on(L.Draw.Event.DRAWSTART, () => {
            if (this.#editableLayer.getLayers().length > 0) {
                this.#editableLayer.clearLayers();
            }
        });

        // Save the geometry of the created feature
        this.#map.on(L.Draw.Event.CREATED, (event) => {
            const layer = event.layer;
            this.#editableLayer.addLayer(layer);
            this.#onCreated(layer.toGeoJSON());
        });

        // Save the geometry of the edited feature
        this.#map.on(L.Draw.Event.EDITED, (event) => {
            const layers = event.layers.getLayers();
            if (0 < layers.length) {
                const layer = layers[0];
                this.#onEdited(layer.toGeoJSON());
            }
        });

        // Erase the geometry of the deleted feature
        this.#map.on(L.Draw.Event.DELETED, () => this.#onDeleted());
    }

    setGeoJson(geoJson) {
        this.#editableLayer.clearLayers();
        const geoJsonLayer = L.geoJSON(geoJson);
        if (geoJsonLayer.getLayers().length > 0) {
            const layer = geoJsonLayer.getLayers()[0];
            this.#editableLayer.addLayer(layer);
            this.#map.fitBounds(this.#editableLayer.getBounds());
            this.#onEdited(layer.toGeoJSON());
        }
    }
}
