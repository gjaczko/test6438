<div class="flex-1 grid grid-cols-4 gap-5 relative">
    @push('styles')
        <!-- Leaflet CSS -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css"/>
        <!-- Leaflet Draw plugin CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.css"/>
    @endpush

    @push('scripts')
        <!-- Leaflet JS -->
        <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
        <!-- Leaflet Draw plugin JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js"></script>
        <!-- Custom Area Editor JS -->
        <script src="{{ asset('js/area-editor.js') }}"></script>
    @endpush

    @script
    <script>
        const geometryTextInput = $wire.$el.querySelector('#geometry');
        const importGeoJsonButton = $wire.$el.querySelector('#importGeoJsonButton');
        const geoJsonFileInput = $wire.$el.querySelector('#geoJsonFile');
        const importKmlButton = $wire.$el.querySelector('#importKmlButton');
        const kmlFileInput = $wire.$el.querySelector('#kmlFile');

        <!-- Initialize the Area Editor -->
        const updateGeometry = geometry => {
            geometryTextInput.value = JSON.stringify(geometry);
            geometryTextInput.dispatchEvent(new Event('input'));
        };
        const areaEditor = new AreaEditor({
            mapId: 'map',
            initialGeometry: JSON.parse($wire.geometry),

            onCreated: (geoJson) => updateGeometry(geoJson.geometry),
            onEdited: (geoJson) => updateGeometry(geoJson.geometry),
            onDeleted: () => updateGeometry(''),
        });

        <!-- Load GeoJson file and set it to the Area Editor -->
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            const geoJson = JSON.parse(event.target.result);
            areaEditor.setGeoJson(geoJson);
        });

        <!-- Read file when the file input changes -->
        geoJsonFileInput.addEventListener('change', (event) => {
            if (0 < event.target.files.length) {
                reader.readAsText(event.target.files[0]);
                event.target.value = '';
                event.target.dispatchEvent(new Event('change'));
            }
        });

        <!-- Open the JSON file selector when the import JSON button is clicked -->
        importGeoJsonButton.addEventListener('click', () => {
            geoJsonFileInput.click();
        });

        <!-- Upload KML file and update the Area Editor -->
        kmlFileInput.addEventListener('change', (event) => {
            if (0 < event.target.files.length) {
                $wire.$upload(
                    'importedFile',
                    event.target.files[0],
                    () => areaEditor.setGeoJson(JSON.parse($wire.get('geometry'))),
                )
            }
        });

        <!-- Open the KML file selector when the import KML button is clicked -->
        importKmlButton.addEventListener('click', () => {
            kmlFileInput.click();
        });
    </script>
    @endscript

    <div>
        <form wire:submit="save" class="block w-full">
            <div class="grid grid-cols-1 gap-3">
                <div class="form-group">
                    <label for="name" class="block text-sm font-medium text-gray-600 dark:text-gray-300">Name</label>
                    <input wire:model.blur="name" type="text" id="name" placeholder="Enter name" class="block w-full mt-1 px-2 py-1 rounded-md border-0 bg-gray-100 dark:bg-gray-700 text-gray-800 dark:text-gray-100 shadow-sm ring-1 ring-inset ring-gray-300 dark:ring-gray-500 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="category" class="block text-sm font-medium text-gray-600 dark:text-gray-300">Category</label>
                    <select wire:model.blur="category_id" id="category" class="block w-full mt-1 px-2 py-1 rounded-md border-0 bg-gray-100 dark:bg-gray-700 text-gray-800 dark:text-gray-100 shadow-sm ring-1 ring-inset ring-gray-300 dark:ring-gray-500 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        <option value="">Select category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('category_id') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="start_date" class="block text-sm font-medium text-gray-600 dark:text-gray-300">Start Date</label>
                    <input wire:model.blur="start_date" type="date" id="start_date" class="block w-full mt-1 px-2 py-1 rounded-md border-0 bg-gray-100 dark:bg-gray-700 text-gray-800 dark:text-gray-100 shadow-sm ring-1 ring-inset ring-gray-300 dark:ring-gray-500 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    @error('start_date') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="end_date" class="block text-sm font-medium text-gray-600 dark:text-gray-300">End Date</label>
                    <input wire:model.blur="end_date" type="date" id="end_date" class="block w-full mt-1 px-2 py-1 rounded-md border-0 bg-gray-100 dark:bg-gray-700 text-gray-800 dark:text-gray-100 shadow-sm ring-1 ring-inset ring-gray-300 dark:ring-gray-500 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                    @error('end_date') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="owner" class="block text-sm font-medium text-gray-600 dark:text-gray-300">Owner</label>
                    <select wire:model.blur="owner_id" id="owner" class="block w-full mt-1 px-2 py-1 rounded-md border-0 bg-gray-100 dark:bg-gray-700 text-gray-800 dark:text-gray-100 shadow-sm ring-1 ring-inset ring-gray-300 dark:ring-gray-500 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
                        <option value="">Select owner</option>
                        @foreach ($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                        @endforeach
                    </select>
                    @error('owner_id') <span class="text-red-500">{{ $message }}</span> @enderror
                </div>
                <input type="hidden" wire:model.live="geometry" id="geometry">
                <input type="file" id="geoJsonFile" accept=".json" class="hidden">
                <input type="file" id="kmlFile" accept=".kml" class="hidden">
                @error('geometry')
                <div class="border-t pt-3 border-gray-200 dark:border-gray-700">
                    <span class="text-red-500">{{ $message }}</span>
                </div>
                @enderror
                @error('importedFile')
                <div class="border-t pt-3 border-gray-200 dark:border-gray-700">
                    <span class="text-red-500">{{ $message }}</span>
                </div>
                @enderror
                <div class="form-buttons flex flex-wrap justify-between gap-3 border-t pt-3 border-gray-200 dark:border-gray-700">
                    <button type="button" id="importGeoJsonButton" class="basis-full rounded-md px-2 py-1 font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Import GeoJson</button>
                    <button type="button" id="importKmlButton" class="basis-full rounded-md px-2 py-1 font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Import KML</button>
                    <button type="submit" wire:loading.attr="disabled" class="rounded-md px-2 py-1 font-medium text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">Save</button>
                    <a href="{{ route('areas.list') }}" class="rounded-md px-2 py-1 font-medium text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">Cancel</a>
                </div>
            </div>
        </form>
    </div>
    <div class="col-span-3">
        <div id="map" wire:ignore class="w-full h-full"></div>
    </div>
    <div wire:loading.class.remove="hidden" wire:target="save" style="z-index: 1001" class="hidden absolute bg-white dark:bg-gray-800 bg-opacity-75 dark:bg-opacity-75 z-10 top-0 left-0 w-full h-full flex items-center justify-center">
        <div class="flex items-center text-gray-800 dark:text-gray-100">
            <span class="text-3xl mr-4">Saving</span>
            <svg class="animate-spin h-8 w-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
                <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
            </svg>
        </div>
    </div>
</div>
