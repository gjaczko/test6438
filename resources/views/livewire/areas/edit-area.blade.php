<div class="flex-1 flex">
    <x-slot name="title">Edit Area</x-slot>
    @livewire('areas.area-form', ['area' => $area])
</div>
