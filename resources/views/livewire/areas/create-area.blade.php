<div class="flex-1 flex">
    <x-slot name="title">Create Area</x-slot>
    @livewire('areas.area-form')
</div>
