<div>
    <x-slot name="title">List of Areas</x-slot>
    <table class="text-gray-700 dark:text-gray-100 border-collapse w-full">
        <thead>
        <tr class="border-b border-gray-200 dark:border-gray-700">
            <th class="p-2 text-left">Name</th>
            <th class="p-2 text-left">Category</th>
            <th class="p-2 text-left">Start Date</th>
            <th class="p-2 text-left">End Date</th>
            <th class="p-2 text-left">Owner</th>
            <th class="p-2 text-right">
                <a class="inline-block px-6 py-2 text-xs text-white bg-green-600 hover:bg-green-400 rounded-md" href="{{ route('areas.create') }}">Create new area</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach ($areas as $area)
            <tr wire:key="{{ $area->id }}" class="border-t border-gray-200 dark:border-gray-700">
                <td class="p-2"><a class="underline hover:text-gray-900 dark:hover:text-white" href="{{ route('areas.edit', $area) }}">{{ $area->name }}</a></td>
                <td class="p-2">{{ $area->category->name }}</td>
                <td class="p-2">{{ $area->start_date }}</td>
                <td class="p-2">{{ $area->end_date }}</td>
                <td class="p-2">{{ $area->owner?->name }}</td>
                <td class="p-2 text-right">
                    <button wire:click="delete({{ $area->id }})" wire:confirm="Are you sure you want to delete {{ $area->name }}?" class="inline-block px-6 py-2 text-xs text-white bg-red-600 hover:bg-red-400 rounded-md">Delete {{ $area->name }}</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
