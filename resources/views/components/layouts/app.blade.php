<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>{{ $title ?? config('app.name') }}</title>

        <!-- Tailwind CSS -->
        <script src="https://cdn.tailwindcss.com"></script>

        <!-- Page CSS -->
        @stack('styles')
    </head>
    <body class="bg-white dark:bg-gray-800 text-gray-800 dark:text-gray-100 font-sans antialiased flex flex-col min-h-screen">
        <header class="p-5 bg-gray-200 dark:bg-gray-700 text-center flex-none">
            <h1 class="font-bold">{{ $title ?? config('app.name') }}</h1>
        </header>
        <main class="p-5 flex-grow flex flex-col">
            @if (session()->has('message'))
                <p class="bg-green-200 text-green-800 p-3 mb-5 rounded">{{ session('message') }}</p>
            @endif
            {{ $slot }}
        </main>
        <!-- Page JS -->
        @stack('scripts')
    </body>
</html>
