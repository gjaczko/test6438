<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            ['name' => 'Category A', 'created_at' => Carbon::now()],
            ['name' => 'Category B', 'created_at' => Carbon::now()],
            ['name' => 'Category C', 'created_at' => Carbon::now()],
            ['name' => 'Category D', 'created_at' => Carbon::now()],
            ['name' => 'Category E', 'created_at' => Carbon::now()],
        ]);
    }
}
