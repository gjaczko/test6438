<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ['name' => 'Client A', 'created_at' => Carbon::now()],
            ['name' => 'Client B', 'created_at' => Carbon::now()],
            ['name' => 'Client C', 'created_at' => Carbon::now()],
        ]);
    }
}
