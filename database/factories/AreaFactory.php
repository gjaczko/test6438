<?php

namespace Database\Factories;

use App\Models\Area;
use App\Models\Category;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Area>
 */
class AreaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->unique()->words(3, true),
            'geometry' => $this->faker->randomElement([
                json_encode([[0, 0], [0, 1], [1, 0]]),
                json_encode([[1, 1], [1, 2], [2, 1]]),
                json_encode([[2, 2], [2, 3], [3, 2]]),
            ]),
            'category_id' => Category::query()->inRandomOrder()->first()->id,
            'start_date' => $this->faker->date(),
            'end_date' => $this->faker->date(),
            'owner_id' => Client::query()->inRandomOrder()->first()->id,
        ];
    }
}
