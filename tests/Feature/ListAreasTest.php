<?php

namespace Tests\Feature;

use App\Models\Area;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ListAreasTest extends TestCase
{
    // Refresh the database before each test
    use RefreshDatabase;

    // Seed the database before each test
    protected bool $seed = true;

    protected Collection $areas;

    public function setUp(): void
    {
        parent::setUp();

        $this->areas = Area::factory()->count(3)->create();
    }

    public function testListPageShowsAreas()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        foreach ($this->areas as $area) {
            $response->assertSee($area->name);
        }
    }

    public function testListPageShowsAreaDetails()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

        // Loop through each area and make sure its details are displayed
        foreach ($this->areas as $area) {
            $response->assertSee($area->name);
            $response->assertSee($area->start_date);
            $response->assertSee($area->end_date);
            $response->assertSee($area->category->name);
            $response->assertSee($area->owner->name);
        }
    }
}
