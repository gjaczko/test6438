<?php

namespace Tests\Feature;

use App\Models\Area;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class EditAreaTest extends TestCase
{
    // Refresh the database before each test
    use RefreshDatabase;

    // Seed the database before each test
    protected bool $seed = true;

    protected Collection $areas;

    public function setUp(): void
    {
        parent::setUp();

        $this->areas = Area::factory()->count(3)->create();
    }

    public function testEditAreaComponentLoadsCorrectly()
    {
        $areaToEdit = $this->areas->random();

        $this->get(route('areas.edit', $areaToEdit->id))
             ->assertSeeLivewire('areas.edit-area')
             ->assertSeeLivewire('areas.area-form');

        Livewire::test('areas.area-form', ['area' => $areaToEdit])
                ->assertSee(['<form', '</form>'], false)
                ->assertSeeInOrder([
                    'Name',
                    'Category',
                    'Start Date',
                    'End Date',
                    'Owner',
                ])
                ->assertSee('Save')
                ->assertSee('Cancel');
    }

    public function testEditAreaValidation()
    {
        $areaToEdit = $this->areas->random();

        Livewire::test('areas.area-form', ['area' => $areaToEdit])
                ->set('name', '')
                ->set('category_id', '')
                ->set('start_date', '')
                ->set('end_date', '')
                ->set('owner_id', '')
                ->set('geometry', '')
                ->call('save')
                ->assertNotDispatched('areaSaved')
                ->assertHasErrors([
                    'name',
                    'category_id',
                    'start_date',
                    'geometry',
                ]);

        $this->assertDatabaseHas('areas', [
            'id' => $areaToEdit->id,
            'name' => $areaToEdit->name,
            'start_date' => $areaToEdit->start_date,
            'end_date' => $areaToEdit->end_date,
            'category_id' => $areaToEdit->category_id,
            'owner_id' => $areaToEdit->owner_id,
        ]);
    }

    public function testCanEditArea()
    {
        $areaToEdit = $this->areas->random();

        Livewire::test('areas.area-form', ['area' => $areaToEdit])
                ->set('name', 'New Name')
                ->set('start_date', '2022-01-01')
                ->set('end_date', '2022-07-01')
                ->call('save')
                ->assertHasNoErrors()
                ->assertDispatched('areaSaved');

        $this->assertDatabaseHas('areas', [
            'id' => $areaToEdit->id,
            'name' => 'New Name',
            'start_date' => '2022-01-01',
            'end_date' => '2022-07-01',
        ]);
    }
}
