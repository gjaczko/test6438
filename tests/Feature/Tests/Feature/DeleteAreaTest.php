<?php

namespace Tests\Feature;

use App\Models\Area;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Livewire\Livewire;

class DeleteAreaTest extends TestCase
{
    // Refresh the database before each test
    use RefreshDatabase;

    // Seed the database before each test
    protected bool $seed = true;

    protected Collection $areas;

    public function setUp(): void
    {
        parent::setUp();

        $this->areas = Area::factory()->count(3)->create();
    }

    public function testCanDeleteArea()
    {
        $areaToDelete = $this->areas->random();

        $this->get(route('areas.list'))
             ->assertSeeLivewire('areas.list-areas');

        Livewire::test('areas.list-areas')
                ->assertSee($areaToDelete->name)
                ->call('delete', $areaToDelete->id)
                ->assertDontSee($areaToDelete->name);

        $this->assertDatabaseMissing('areas', ['id' => $areaToDelete->id]);
    }
}
