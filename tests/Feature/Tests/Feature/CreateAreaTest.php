<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class CreateAreaTest extends TestCase
{
    // Refresh the database before each test
    use RefreshDatabase;

    // Seed the database before each test
    protected bool $seed = true;

    public function testCreateAreaComponentLoadsCorrectly()
    {
        $this->get(route('areas.create'))
             ->assertSeeLivewire('areas.create-area')
             ->assertSeeLivewire('areas.area-form');

        Livewire::test('areas.create-area')
                ->assertSee(['<form', '</form>'], false)
                ->assertSeeInOrder([
                    'Name',
                    'Category',
                    'Start Date',
                    'End Date',
                    'Owner',
                ])
                ->assertSee('Save')
                ->assertSee('Cancel');
    }

    public function testCreateAreaValidation()
    {
        Livewire::test('areas.area-form')
                ->set('name', '')
                ->set('category_id', '')
                ->set('start_date', '')
                ->set('end_date', '')
                ->set('owner_id', '')
                ->set('geometry', '')
                ->call('save')
                ->assertNotDispatched('areaSaved')
                ->assertHasErrors([
                    'name',
                    'category_id',
                    'start_date',
                    'geometry',
                ]);

        $this->assertDatabaseCount('areas', 0);
    }

    public function testCanCreateArea()
    {
        Livewire::test('areas.area-form')
                ->set('name', 'Test Area')
                ->set('category_id', 1)
                ->set('start_date', '2024-01-01')
                ->set('end_date', '2024-12-31')
                ->set('owner_id', 1)
                ->set('geometry', json_encode([
                    'type' => 'Polygon',
                    'coordinates' => [
                        [
                            [-6.371985, 49.582226],
                            [1.054915, 50.652943],
                            [2.241239, 51.781436],
                            [2.241325, 52.855864],
                            [1.011254, 53.46189],
                            [-1.625891, 56.36525],
                            [-1.191332, 57.774518],
                            [-2.725348, 58.101105],
                            [-1.713697, 59.489726],
                            [-2.679272, 59.623325],
                            [-7.777837, 58.240164],
                            [-7.954499, 57.279043],
                            [-6.899158, 56.072035],
                            [-10.986186, 54.265224],
                            [-10.466841, 52.961875],
                            [-11.477915, 52.106505],
                            [-9.895364, 50.930738],
                            [-5.763147, 52.295042],
                            [-6.371985, 49.582226],
                        ],
                    ],
                ]))
                ->call('save')
                ->assertHasNoErrors()
                ->assertDispatched('areaSaved');

        $this->assertDatabaseHas('areas', [
            'name' => 'Test Area',
            'category_id' => 1,
            'start_date' => '2024-01-01',
            'end_date' => '2024-12-31',
            'owner_id' => 1,
        ]);
    }
}
