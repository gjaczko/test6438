<?php

namespace App\Services;

use Exception;
use SimpleXMLElement;

class KmlParser
{
    private SimpleXMLElement $kml;

    public function parseFile($kmlFile): static
    {
        return $this->parse(file_get_contents($kmlFile));
    }

    public function parse(string $kmlString): static
    {
        $this->kml = simplexml_load_string($kmlString);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getGeometry(): array
    {
        return [
            'type' => 'Polygon',
            'coordinates' => [$this->getCoordinates()],
        ];
    }

    /**
     * @return float[][2]
     * @throws Exception
     */
    public function getCoordinates(): array
    {
        // Validate that the KML file is valid and has a Polygon
        if (!isset($this->kml->Document->Placemark)) {
            throw new Exception('Invalid KML file.');
        }
        if (!isset($this->kml->Document->Placemark->Polygon)) {
            throw new Exception('Missing Polygon in KML file.');
        }
        if (!isset($this->kml->Document->Placemark->Polygon->outerBoundaryIs->LinearRing->coordinates)) {
            throw new Exception('Missing coordinates in KML file.');
        }

        // Get the coordinates from the KML file
        $coordinatesString = trim($this->kml->Document->Placemark->Polygon->outerBoundaryIs->LinearRing->coordinates);

        // Convert the coordinates to an array of longitude and latitude pairs
        return array_map(
            fn($point) => [(float)$point[0], (float)$point[1]],
            array_filter(
                array_map(
                    fn($pointString) => explode(',', $pointString),
                    explode(' ', $coordinatesString)
                ),
                fn($point) => isset($point[1])
            )
        );
    }
}
