<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'geometry',
        'category_id',
        'start_date',
        'end_date',
        'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(Client::class, 'owner_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
