<?php

namespace App\Livewire\Areas;

use Illuminate\Contracts\View\View;
use Livewire\Attributes\On;
use Livewire\Component;

class CreateArea extends Component
{
    #[On('areaSaved')]
    public function saved()
    {
        session()->flash('message', 'Area created successfully.');

        return redirect()->route('areas.list');
    }

    public function render(): View
    {
        return view('livewire.areas.create-area');
    }
}
