<?php

namespace App\Livewire\Areas;

use App\Models\Area;
use Livewire\Attributes\On;
use Livewire\Component;

class EditArea extends Component
{
    public Area $area;

    public function mount(Area $area): void
    {
        $this->area = $area;
    }

    #[On('areaSaved')]
    public function saved()
    {
        session()->flash('message', 'Area updated successfully.');

        return redirect()->route('areas.list');
    }

    public function render()
    {
        return view('livewire.areas.edit-area');
    }
}
