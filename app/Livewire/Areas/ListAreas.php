<?php

namespace App\Livewire\Areas;

use App\Models\Area;
use Livewire\Component;

class ListAreas extends Component
{
    public function render()
    {
        return view('livewire.areas.list-areas', [
            'areas' => Area::with(['category', 'owner'])->get(),
        ]);
    }

    public function delete(Area $area)
    {
        $area->delete();

        session()->flash('message', 'Area deleted successfully.');
    }
}
