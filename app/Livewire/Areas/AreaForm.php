<?php

namespace App\Livewire\Areas;

use App\Models\Area;
use App\Models\Category;
use App\Models\Client;
use App\Services\KmlParser;
use Illuminate\Contracts\View\View;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Livewire\WithFileUploads;

class AreaForm extends Component
{
    use WithFileUploads;

    public Area|null $area = null;

    #[Validate]
    public $name;
    #[Validate]
    public $geometry;
    #[Validate]
    public $category_id;
    #[Validate]
    public $start_date;
    #[Validate]
    public $end_date;
    #[Validate]
    public $owner_id;

    public $importedFile;

    public function mount(Area $area = null): void
    {
        if (isset($area)) {
            $this->area = $area;
            $this->name = $area->name;
            $this->geometry = $area->geometry;
            $this->category_id = $area->category_id;
            $this->start_date = $area->start_date;
            $this->end_date = $area->end_date;
            $this->owner_id = $area->owner_id;
        } else {
            $this->area = new Area();
        }
    }

    public function validationAttributes(): array
    {
        return [
            'category_id' => 'category',
            'owner_id' => 'owner',
        ];
    }

    public function rules(): array
    {
        return [
            'name' => isset($this->area) ?
                ['required', 'min:3', 'max:100', 'unique:areas,name,' . $this->area->id] :
                ['required', 'min:3', 'max:100', 'unique:areas,name'],
            'geometry' => ['required', 'json', 'min:3'],
            'category_id' => ['required', 'exists:categories,id'],
            'start_date' => ['required', 'date'],
            'end_date' => ['nullable', 'date', 'after:start_date'],
            'owner_id' => ['nullable', 'exists:clients,id'],
        ];
    }

    public function messages(): array
    {
        return [
            'geometry.required' => 'The :attribute is required.',
            'geometry.min' => 'The :attribute cannot be empty.',
        ];
    }

    public function save()
    {
        $this->validate();

        $this->area->fill([
            'name' => $this->name,
            'geometry' => $this->geometry,
            'category_id' => $this->category_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'owner_id' => $this->owner_id,
        ])->save();

        $this->dispatch('areaSaved');
    }

    public function updatedImportedFile(KmlParser $kmlParser): void
    {
        $this->validate([
            'importedFile' => ['required', 'file', 'max:100'],
        ]);

        try {
            $geometry = $kmlParser->parseFile($this->importedFile->getRealPath())->getGeometry();
            $this->geometry = json_encode($geometry);
        } catch (\Exception $e) {
            $this->addError('geometry', $e->getMessage());
        }
    }

    public function render(): View
    {
        return view('livewire.areas.area-form', [
            'categories' => Category::all(),
            'clients' => Client::all(),
        ]);
    }
}
